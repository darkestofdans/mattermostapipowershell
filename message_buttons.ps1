<# This form was created using POSHGUI.com  a free online gui designer for PowerShell
.NAME
    Untitled
#>

Add-Type -AssemblyName System.Windows.Forms
[System.Windows.Forms.Application]::EnableVisualStyles()

$MessageButtons                  = New-Object system.Windows.Forms.Form
$MessageButtons.ClientSize       = '331,241'
$MessageButtons.text             = "Form"
$MessageButtons.BackColor        = "#b8e986"
$MessageButtons.TopMost          = $false

$ButtonTest1                  = New-Object system.Windows.Forms.Button
$ButtonTest1.BackColor        = "#000000"
$ButtonTest1.text             = "Test Button 1"
$ButtonTest1.width            = 141
$ButtonTest1.height           = 89
$ButtonTest1.location         = New-Object System.Drawing.Point(14,11)
$ButtonTest1.Font             = 'Microsoft Sans Serif,24'
$ButtonTest1.ForeColor        = "#f8e71c"
$ButtonTest1.Add_Click({New-MMMessage off-topic test})

$Button2                         = New-Object system.Windows.Forms.Button
$Button2.BackColor               = "#000000"
$Button2.text                    = "Test 2"
$Button2.width                   = 141
$Button2.height                  = 89
$Button2.location                = New-Object System.Drawing.Point(161,11)
$Button2.Font                    = 'Microsoft Sans Serif,23'
$Button2.ForeColor               = "#f8e71c"
$Button2.Add_Click({New-MMMessage off-topic test2})

$Button3                         = New-Object system.Windows.Forms.Button
$Button3.BackColor               = "#000000"
$Button3.text                    = "Major"
$Button3.width                   = 141
$Button3.height                  = 89
$Button3.location                = New-Object System.Drawing.Point(14,105)
$Button3.Font                    = 'Microsoft Sans Serif,22'
$Button3.ForeColor               = "#f8e71c"
$Button3.Add_Click({New-MMMessage town-square test3})

$Button4                         = New-Object system.Windows.Forms.Button
$Button4.BackColor               = "#000000"
$Button4.text                    = "Good morning Dave"
$Button4.width                   = 141
$Button4.height                  = 89
$Button4.location                = New-Object System.Drawing.Point(161,105)
$Button4.Font                    = 'Microsoft Sans Serif,10'
$Button4.ForeColor               = "#f8e71c"
$Button4.Add_Click({New-MMMessage off-topic goodmorningdave})

$MessageButtons.controls.AddRange(@($ButtonTest1,$Button2,$Button3,$Button4))

#Write your logic code here

function New-MMMessage {
    [CmdletBinding()]        
    Param
     (
        [parameter(Mandatory=$true)]
        [String]$inchannelname,
        [parameter(Mandatory=$true)]
        [String]$inmessage
       )
    try {
    switch ($inchannelname) {
            #define the channels you'll use here. You'll need to use searchchannel.ps1 to get the channel IDs for each channel
            off-topic {$channelname = "z4eu4fkao7y-Off-Topic-ID-jq11b4g3ety"}
            town-square {$channelname = "q464fkao5-Town-Square-ID-mg4jq4g3ejj"}
            default {Out-host "Incorrect channel name, check spelling"; break}
        }
        switch ($inmessage){
            #define the messages you'll want to use.
            test {$messagename = "Message 1"}
            test2 {$messagename = "Message 2"}
            test3 {$messagename = 'Message 3\n is multiple lines\n#### and formatted'}
            goodmorningdave {$messagename = "Good morning Dave"}
            default {Out-host "Incorrect message name, check spelling"; break}
        }
    } catch { "InvalidArgument"; break}
    
    #read in the saved url from the login_store_session.ps1 script
    $secureurl = get-content -path "$HOME\Documents\mmurl.txt" | ConvertTo-SecureString
    $bstrurl = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($secureurl)
    $url = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstrurl)

    $postsurl = $url + '/api/v4/posts'
    #$directurl = $url + '/api/v4/channels/direct' # Currently not used, may implement direct messages later
    
    #Read in the token and convert it back to plaintext
    $securetoken = get-content -path "$HOME\Documents\mmtoken.txt" | ConvertTo-SecureString
    $bstrtoken = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($securetoken)
    $plaintoken = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstrtoken)
    $headers = @{'Authorization' = 'Bearer ' + $plaintoken}
    $payload = '{"channel_id":"' + $channelname + '","message":"' + $messagename + '"}'

    Invoke-RestMethod -Uri $postsurl -ContentType "application/json" -Method 'post' -Body $payload -Header $headers
    
}

[void]$MessageButtons.ShowDialog()