# Ask user for what team they'd like information about
$teamname = Read-host 'What team name are you searching for?'

# Put the string together
$payload = '{"term":"' + $teamname + '"}'

# read in the URL
$secureurl = get-content -path "$HOME\Documents\mmurl.txt" | ConvertTo-SecureString
$bstrurl = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($secureurl)
$url = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstrurl)

$teamsearchurl = $url + '/api/v4/teams/search'

$securetoken = get-content -path "$HOME\Documents\mmtoken.txt" | ConvertTo-SecureString
#convert it back to plaintext
$bstrtoken = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($securetoken)
$plaintoken = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstrtoken)

# Place your API token here
$headers = @{'Authorization' = 'Bearer ' + $plaintoken}

# Invoke-RestMethod, now we'll have the team info and can use the team_id to make a query about the channel
$teaminfo = Invoke-RestMethod -uri $teamsearchurl -ContentType  "application/json" -Method 'post' -Body $payload -Header $headers

# Ask user about the channel, comment out the next line and the query will return info on all channels.
$channelname = Read-host "What channel on team $teamname are you searching for?"

# Build the string
$payload2 = '{"term":"' + $channelname + '"}'

# Build the URL, put your URL here
$channelsearchurl = $url + '/api/v4/teams/' + $teaminfo.id + '/channels/search'

# Invoke-RestMethod
Invoke-RestMethod -uri $channelsearchurl -ContentType  "application/json" -Method 'post' -Body $payload2 -Header $headers

<# Returns

id                : z4ei4fkao6y8mj4jq12b4g3tty
create_at         : 1569720040311
update_at         : 1569720040311
delete_at         : 0
team_id           : rn3ghw4zbg3rbbcdpao5ee1mekttylh
type              : O
display_name      : Off-Topic
name              : off-topic
header            :
purpose           :
last_post_at      : 1570650083169
total_msg_count   : 23567
extra_update_at   : 0
creator_id        :
scheme_id         :
props             :
group_constrained :

#>