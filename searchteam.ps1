# Ask user for what team they'd like information about
$teamname = Read-host 'What team name are you searching for?'

# Put the string together
$payload = '{"term":"' + $teamname + '"}'

# read in the URL
$secureurl = get-content -path "$HOME\Documents\mmurl.txt" | ConvertTo-SecureString
$bstrurl = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($secureurl)
$url = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstrurl)

$teamsearchurl = $url + '/api/v4/teams/search'

$securetoken = get-content -path "$HOME\Documents\mmtoken.txt" | ConvertTo-SecureString
#convert it back to plaintext
$bstrtoken = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($securetoken)
$plaintoken = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstrtoken)

# Place your API token here
$headers = @{'Authorization' = 'Bearer ' + $plaintoken}

# Invoke-RestMethod, now we'll have the team info and can use the team_id to make a query about the channel
Invoke-RestMethod -uri $teamsearchurl -ContentType  "application/json" -Method 'post' -Body $payload -Header $headers
<#  The API will return the following.  Now you know the id to use in other API calls.

id                : rnggw8zbg3rbdpao7ee3meknyh
create_at         : 1549830040311
update_at         : 1549830040311
delete_at         : 0
display_name      : Test team
name              : test-team
description       :
email             : example@example.com
type              : O
company_name      :
allowed_domains   :
invite_id         : c7q5s6ms2ygybbcieceg9o3pe2
allow_open_invite : False
scheme_id         :
group_constrained :
#>
