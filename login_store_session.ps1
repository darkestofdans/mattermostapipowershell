$loginname = Read-host "Enter username"
$secret = Read-host "Enter password" -AsSecureString
$url = Read-Host "Enter Mattermost website such as https://chat.example.com"

# Convert the password from the secure string on input into plaintext
$bstr = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($secret)
$pass = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstr)

# Very weak error incorrection for URL in case someone types https://url/ instead of https://url
$url = $url.TrimEnd('/')

# Combine the string
$payload=  '{"login_id":"' + $loginname + '","password":"' + $pass + '"}' 
$loginurl = $url + '/api/v4/users/login'

# Very weak attempt at providing feedback if the URL or creds are wrong while submitting the login to the API
try {
    $response = Invoke-WebRequest -Uri $loginurl -Method 'Post' -Body $payload -SessionVariable 'Session'
} catch {'api.user.login.invalid_credentials_email_username','The remote name could not be resolved'; Write-Warning 'Invalid creds or URL'; BREAK }

# Take the session token, store it in a file as a secure string, we'll use it for the other scripts
$response.headers.token | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString | Out-File "$HOME\Documents\mmtoken.txt" -Force
Write-Host 'Session token saved in $HOME\Documents\mmtoken.txt'

# Store the URL too.  Using secrure string to prevent accidental changes to the URL.
$url | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString | Out-File "$HOME\Documents\mmurl.txt" -Force
Write-Host "Mattermost URL saved in $HOME\Documents\mmurl.txt"