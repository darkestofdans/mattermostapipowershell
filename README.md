# mattermostapipowershell

Using the Mattermost API with PowerShell

The simple mistake I made that I'm documenting to help others.

channel_name -ne channel_id

First use the API to get the team_id, channel_id, and user_id and then you'll
have much more success.

When you get an error like this it is because you're probably trying to post to the channel name and not channel_id: 

```    + CategoryInfo          : InvalidOperation: (System.Net.HttpWebRequest:HttpWebRequest) [Invoke-RestMethod], WebException
    + FullyQualifiedErrorId : WebCmdletWebResponseException,Microsoft.PowerShell.Commands.InvokeRestMethodCommand
Invoke-RestMethod : {"id":"api.context.permissions.app_error","message":"You do not have the appropriate permissions","detailed_error":"userId=fmrtxxxx544xadg, 
permission=create_post","request_id":"xxxx56deeage444","status_code":403}
```

searchteam.ps1 - Use to find team_id

searchchannel.ps1 - Use to find channel_id

login_store_session.ps1 - Logs into Mattermost and stores the URL and session
    token in $HOME\Documents\
    
new_mmmessage.ps1 - Edit the script with the appropriate channel IDs and then
    create some messages.
    Run the script and use New-MMMessage channelname message

new-mmmessage.psm1 - Import-Module new-mmmessage.psm1
    New-MMessage channelname message
    
message_buttons.ps1 - POSHGUI buttons for New-MMMessage buttons

## How to use

1.) Log in using login_store_session.ps1

You'll need username/password/url.  Url needs to be https://example.com

This script will save your session token to be used for other scripts and the URL.

2.) Use query_channels_alreadyloggedin.ps1  This script will export the channels for that team into a CSV.  The POSHGUI buttons will still need to be hardcoded, but the New-MMMessage module will read in the channels from this list.  Channel IDs are saved in "$HOME\Documents\channellistquery.csv"

3.) Search for channel(s), write down IDs for later, or pipe it to a file by editting the script.

4.) Edit the new_mmmessage.ps1 or new-mmmessage.psm1 to contain any custom messages you want.  Edit message_buttons.ps1 for channels IDs and messages.

5.) Run the script(s) or import the psm1 module if you're that sort of thing.  Post messages with the click of a button or New-MMMessage channelname messagename

## Why I made New-MMMessage

I often have to post the same predefined message in certain channels.  I wanted
a way to quickly post reused messages that was customizable and didn't require
any server changes or a bot for slash commands.

Trying to clean up the commits.