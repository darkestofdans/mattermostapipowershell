#This function will post pre-defined messages to pre-defined channels.  Use by New-MMMessage channel message
#Write-Host 'After defining channels and messages in the script use by New-MMMessage channelname message'
$hashtable = @{}
#Import the channel list csv and populate the hash table
$csv = Import-Csv "$HOME\Documents\channellistquery.csv"
foreach ($item in $csv) {
    $hashtable.add($item.key, $item.value)
 }
function New-MMMessage {
    [CmdletBinding()]        
    Param
     (
        [parameter(Mandatory=$true)]
        [String]$inchannelname,
        [parameter(Mandatory=$true)]
        [String]$inmessage
       )
    
    try {

        switch ($inmessage){
            #define the messages you'll want to use here.  
            test {$messagename = "It's 22:42"}
            test2 {$messagename = "Message 2"}
            test3 {$messagename = 'Message 3\n is multiple lines\n#### and formatted'}
            goodmorningdave {$messagename = 'Good morning Dave'}
            default {Out-host "Incorrect message name, check spelling"; break}
        }
        
    } catch { "InvalidArgument"; break}

    #read in the saved url from the login_store_session.ps1 script
    $secureurl = get-content -path "$HOME\Documents\mmurl.txt" | ConvertTo-SecureString
    $bstrurl = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($secureurl)
    $url = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstrurl)
    
    $postsurl = $url + '/api/v4/posts'
    # $directurl = $url + '/api/v4/channels/direct' # For use when I add direct message support
    
    #Read in the stored URL
    $securetoken = get-content -path "$HOME\Documents\mmtoken.txt" | ConvertTo-SecureString
    #convert it back to plaintext
    $bstrtoken = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($securetoken)
    $plaintoken = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstrtoken)
    $headers = @{'Authorization' = 'Bearer ' + $plaintoken}
    $payload = '{"channel_id":"' + $hashtable.Item("$inchannelname") + '","message":"' + $messagename + '"}'

    Invoke-RestMethod -Uri $postsurl -ContentType "application/json" -Method 'post' -Body $payload -Header $headers
    
}

