$secureurl = get-content -path "$HOME\Documents\mmurl.txt" | ConvertTo-SecureString
$bstrurl = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($secureurl)
$url = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstrurl)
    #Read in the stored URL
$securetoken = get-content -path "$HOME\Documents\mmtoken.txt" | ConvertTo-SecureString
    #convert it back to plaintext
$bstrtoken = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($securetoken)
$plaintoken = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstrtoken)
$headers = @{'Authorization' = 'Bearer ' + $plaintoken}

# Ask user for what team they'd like information about
$teamname = Read-host 'What team name are you searching for?'

# Put the string together
$teampayload = '{"term":"' + $teamname + '"}'
$teamsearchurl = $url + '/api/v4/teams/search'

# Invoke-RestMethod, now we'll have the team info and can use the team_id to make a query about the channel
$teaminfo = Invoke-RestMethod -uri $teamsearchurl -ContentType  "application/json" -Method 'post' -Body $teampayload -Header $headers


# Build the string
$channelpayload = '{"term":""}'

# Build the URL, put your URL here
$channelsearchurl = $url + '/api/v4/teams/' + $teaminfo.id + '/channels/search'

# Invoke-RestMethod, export CSV
$channellist = Invoke-RestMethod -uri $channelsearchurl -ContentType  "application/json" -Method 'post' -Body $channelpayload -Header $headers |`
Select-Object -Property $_.name

$channelhashtable = @{}
$channellist | Foreach { $channelhashtable[$_.name] = $_.id }
$channelhashtable.GetEnumerator() | sort-object Name | Export-Csv -Path "$HOME\Documents\channellistquery.csv" -Force -NoTypeInformation
Write-host "Channel list saved in $HOME\Documents\channellistquery.csv"